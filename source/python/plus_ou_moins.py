from random import randint

n = randint (1,100)
rep = False
nb_essais = 5

while rep == False and nb_essais > 0:
    prop = int(input("proposer un nombre entre 1 et 100: "))
    nb_essais = nb_essais - 1
    if prop == n:
        rep = True
    else:
        if prop < n:
            print("plus grand")
        else:
            print("moins grand")
if rep == True:
    print(f"Gagné le nombre cherché est bien {n} !")
elif nb_essais == 0:
    print(f"Vous avez perdu en dépassant le nombre d'essais autorisés ! Le nombre cherché est {n}.")

