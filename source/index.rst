Boucle conditionnelle
=======================

.. figure:: img/image_intro.jpeg
    :align: center
    :width: 600
    :class: margin-bottom-16
    
En Python, il existe 2 types de boucle:

-   La boucle bornée ``for``
-   La boucle conditionnelle ``while``

Contrairement à une boucle bornée qui exécute les instructions contenues dans la boucle un nombre défini de fois, la boucle conditionnelle exécute les instructions tant qu'une condition est vraie.

.. toctree::
    :maxdepth: 1
    :hidden:
    
    content/boucle_while.rst
    content/exercice_1.rst
    content/exercice_2.rst
