Exercices complémentaires
=============================

.. exercice::

	On souhaite réaliser le dessin ci-dessous en affichant le caractère ``X``. Il est composé de 10 	lignes et 10 colonnes.

	.. image:: ../img/carreX.png
		:alt: image
		:align: center

	#.	Créer une première boucle qui permet de créer la première ligne du dessin. 
	#.	Créer une boucle qui va répéter 10 fois la boucle précédente. On dit que les boucles sont **imbriquées**.

		.. note::
			
			Pour afficher du texte sur une même ligne, il faut ajouter le paramètre ``end`` à la fonction **print** : ``print(message,end="")``.

	#.	Modifier votre code pour que le nombre de lignes et colones soit saisi par l'utilisateur.

.. exercice::

	Réaliser les dessins suivants de 10 lignes et 10 colonnes:

	#.	
	
		.. image:: ../img/carreX2.png
			:alt: image
			:align: center
			:class: margin-bottom-8

	#.		

		.. image:: ../img/carreX4.png
			:alt: image
			:align: center
			:class: margin-bottom-8

	#.	

		.. image:: ../img/carreX6.png
			:alt: image
			:align: center
			:class: margin-bottom-8

.. exercice::
		
	#.	Écrire un premier programme qui calcule et affiche	:math:`1 + 2 + 3 + \ldots + 100`.
	#.	Écrire un second programme qui calcule et affiche :math:`1 \times 2 \times \ldots \times 100`.
	#.	Modifier les deux programmes précédents pour calculer et afficher les résultats jusqu’à un nombre :math:`N` saisi par l’utilisateur.

.. exercice::
		
	Les programmes seront appliqués sur la chaine de caractères suivante : "Le langage python a été créé par Guido Van Rossum".

	#.	Écrire un programme qui affiche les voyelles contenues dans la chaine de caractères.
	#.	Écrire un programme qui affiche la chaine de caractères écrite à l'envers (lettre par lettre).

.. exercice::

	Lorsqu'on saisit dans l'interpréteur Python la fonction ``bin(9)``, on obtient l'écriture binaire du nombre 9 qui est ``1001``.
	
	L'objectif est de convertir l'écriture décimale d'un nombre entier positif en écriture binaire. Pour cela, on utilise la méthode des divisions successives.
	
	On utilise les variables :
	
	-	``n`` pour le nombre entier positif à convertir de décimal à binaire,
	-	``b`` pour l'écriture binaire obtenue qui sera de type ``str``,
	-	``r`` pour les restes des divisions entières.

	On donne les trois étapes essentielles du programme à écrire.

	-	Le nombre ``n`` doit être divisé par 2 jusqu'à devenir nul. Il faut écrire une boucle qui fait les calculs tant que la variable ``n`` est différente de ``0``.
	-	Il faut calculer les divisions entières en mémorisant le reste dans la variable ``r`` et le quotient dans la variable ``n``.
	-	Le nombre binaire ``b`` est obtenu par concaténation des restes ``r`` comme chaine de caractères.

	Écrire un code en Python qui donne le même résultat que la fonction ``bin`` sans l'utiliser.