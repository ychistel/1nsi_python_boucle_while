Exercices
============

.. exercice::

    Indiquer la valeur des variables après exécution du code !

    #.  Variable ``a``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 1-3

    #.  Variable ``b``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 5-7

    #.  Variables ``c`` et ``d``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 9-13
            
    #.  Variable ``e``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 15-17

    #.  Variable ``f``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 19-21

    #.  Variable ``g``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 23-25

    #.  Variables ``h`` et ``i``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 27-31

    #.  Variables ``j`` et ``k``:

        .. literalinclude:: ../python/exercice_2.py
            :lines: 33-40

.. exercice::

    Écrire des boucles conditionnelles pour :

    #.  Afficher dans l'ordre : 8, 7, 6, 5, 4, 3, 2, 1, 0
    #.  Afficher dans cet ordre : 1, 3, 5, 7, 9, 11
    #.  Afficher dans cet ordre : 1, 10, 100, 1000, 10000, 100000, 1000000
    #.  Afficher dans cet ordre : 1, -1, 1, -1, 1, -1, 1, -1, 1

    .. tip::

        Pour afficher les valeurs sur une même ligne, il suffit d'ajouter l'argument ``end = ', '`` à la fonction ``print``.
        
        Par exemple : ``print(i, end=', ')``

.. exercice::

    Le jeu du **plus** ou **moins** consiste à deviner un nombre entier choisi au hasard par la machine entre 1 et 100. A chaque proposition:

    -  si on trouve le nombre, on a gagné et le jeu prend fin.
    -  si on ne trouve pas le nombre, on reçoit l'information **plus grand** ou **moins grand** et le jeu continue.

    On doit écrire un code en Python qui modélise ce jeu.

    #.  Voici les instructions à ajouter à votre programme pour choisir aléatoirement un nombre entier:

        -   Au début du programme

            .. code:: python

                from random import randint

        -   Pour créer un nombre aléatoire entre 1 et 100 dans la variable ``n``:

            .. code:: python

                n = randint(1,100)

    #.  Créer la variable ``rep`` qui est un booléen de valeur ``False``.
        
    #.  La saisie du nombre est enregistrée dans la variable ``prop``. 

        Elle se réalise avec l'instruction: ``prop = int(input("proposer un nombre entre 1 et 100: "))``
        
    #.  Écrire une boucle ``while`` qui propose la saisie d'un nombre tant qu'on n'a pas trouvé le nombre à deviner.
    
        Cette boucle affiche "gagné" si le nombre est trouvé, sinon affiche "plus grand" si le nombre cherché est plus grand et affiche "plus petit" si le nombre cherché est plus petit.

    #.  Modifier votre programme pour limiter le nombre de tentatives. On ajoutera la variable ``nb_essais`` initialisée à ``5`` que l'on désincrémente de 1 à chaque tentative.

    #.  Quel est le nombre minimum de tentatives à accorder au joueur pour qu'il gagne à tous les coups ? 

