TP : les opérateurs logiques
=============================

Les opérateurs logiques sont des opérations sur des valeurs booléennes c'est à dire sur des valeurs égales à ``True`` ou ``False``.

.. note::

    Pour simplifier l'écriture des valeurs ``True`` et ``False``, on les associe respectivement aux valeurs binaires ``1`` et ``0``.

On va s'intéresser à trois opérateurs booléens : ``not``, ``and`` et ``or``.

L'opérateur not
---------------------

Cet opérateur permet d'obtenir la négation d'une valeur booléenne. 

#.  Quelle est la valeur de l'expression ``not True`` ? et de ``not False`` ?
#.  On représente les différents cas de figure dans un tableau appelé **table de vérité**. Recopier et compléter cette table:

    .. table::
        :align: center
        :width: 20
        
        ===== ======
        x     not(x)
        ===== ======
        0 
        1 
        ===== ======

#.  Écrire des tests équivalents aux tests suivants en utilisant l'opérateur ``not``:

    a.  ``a != 5``
    b.  ``a >= 0``
    c.  ``a < 4``

L'opérateur and
--------------------

L'opérateur ``and`` s'utilise avec 2 valeurs booléennes. Par exemple, si ``x`` et ``y`` sont deux valeurs booléennes, alors l'expression ``x and y`` renvoie une valeur booléenne.

La valeur booléenne ``x and y`` est vraie si et seulement si ``x`` vaut vraie et ``y``  vaut vraie en même temps.

#.  Quelle est la valeur de l'expression ``True and False`` ? et de ``True and True``?
#.  Recopier et compléter la table de vérité de l'opérateur ``and`` selon les valeurs de ``x`` et ``y``.

    .. table::
        :align: center
        
        ===== ===== =======
        x     y     x and y
        ===== ===== =======
        0     0            
        0     1            
        1     0            
        1     1            
        ===== ===== =======

#.  La variable ``a`` est de type entier. Écrire des tests équivalents aux test suivants avec l'opérateur ``and``:

    a.  ``a == 5``
    b.  ``0 < a < 10``

L'opérateur or
-------------------

L'opérateur ``or`` s'utilise avec 2 valeurs booléennes. Par exemple, si ``x`` et ``y`` sont deux valeurs booléennes, alors l'expression ``x or y`` renvoie une valeur booléenne.

La valeur booléenne ``x or y`` est vraie si et seulement si l'une au moins des valeurs ``x`` ou ``y`` est vraie.

#.  Quelle est la valeur de l'expression ``True or False`` ? et de ``True or True``?
#.  Recopier et compléter la table de vérité de l'opérateur ``or`` selon les valeurs de ``x`` et ``y``.

    .. table::
        :align: center
        
        ===== ===== =======
        x     y     x or y
        ===== ===== =======
        0     0            
        0     1            
        1     0            
        1     1            
        ===== ===== =======

#.  La variable ``a`` est de type entier. Écrire des tests équivalents aux tests suivants avec l'opérateur ``or``:

    a.  ``a != 5``
    b.  ``0 <= a``

Exercice d'application
---------------------------

Une année est bissextile si elle est divisible par 4 mais pas par 100 ou si elle est divisible par 400.

La variable ``n`` est de type entier et représente une année.

#.  Écrire un test qui vérifie que l'année ``n`` est divisible par 4.
#.  Écrire un test qui vérifie que l'année ``n`` n'est pas divisible par 100.
#.  Écrire un test qui vérifie que l'année ``n`` est divisible par 400.
#.  En utilisant les opérateurs logiques, écrire en une seule condition qui est vraie si l'année ``n`` est bissextile et fausse dans le cas contraire.
#.  Écrire un code en Python qui demande la saisie d'une année et qui affiche si elle est bissextile ou non. 
